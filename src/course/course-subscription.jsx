import React,{ useEffect, useState } from 'react';
import toast,{ Toaster } from "react-hot-toast";
import { useParams } from "react-router-dom";
import axios from "axios";
import CourseSurvey from "./course-aditional-elements/course-survey";
import CourseComments from "./course-aditional-elements/course-comments";
import { destructureDate, destructureHours, searchDataInstructor, getInstructorName } from "../utilities/utilities";
import { useUserData } from "../stores/user.data.store";
import SearchBar from "../components/searchBar";
import TooglesUser from "../components/tooglesUser";
import AsideNav from "../components/aside-nav/aside-nav";
import SuscriptionPage from "../components/pages-messages/sectionSuscribed";
import SectionWaiting from "../components/pages-messages/sectionWaiting";
import 'ldrs/tailspin'

function CourseSubscription() {
    const userEmail = useUserData((state) => state.userEmail);
    const { idCurso } = useParams();
    const [dataCourse, setDataCourse] = useState({});
    const [dataInstructor, setDataInstructor] = useState({});
    const [currentDate, setCurrentDate] = useState(new Date());
    const [userIsSubscribed, setUserIsSubscribed] = useState(false);
    const [courseDateIsAvailable, setCourseDateIsAvailable] = useState(null);

    const subscribeCourse = async () => {
        const objectDataSubscripcion = {
            correoUsuario: userEmail, 
            idCurso: idCurso
        }
        try {
            const response = await axios.post('http://localhost:3000/suscripcion/curso', {
                dataSubscripcion: objectDataSubscripcion
            });

            if(response.status === 200){
                setUserIsSubscribed(true);
                toast.success(`Inscrito a ${dataCourse.titulo} con éxito`);
            }
        }catch(err){
            toast.error(`Error al inscribirse, pruebe más tarde`);
        }
    }

    const checkUserIsSubscribed = async () => {
        try {
            const response = await axios.get(`http://localhost:3000/validar/suscripcion/${userEmail}/${idCurso}`);
            setUserIsSubscribed(response.data);
        } catch (error) {
            
        }
    }
    
    async function getDataInstructor(instructor) {
        if (instructor) {
          try {
            const instructorData = await searchDataInstructor(instructor);
            if (instructorData && instructorData !== null) {
              setDataInstructor(instructorData);
            }
          } catch (error) {
            toast.error("Error al obtener datos del instructor");
          }
        }
      }
      

    useEffect(() => {
        async function getDataCourse() {
            try {
            const dataCourseResponse = await axios.get(`http://localhost:3000/obtener/curso/${idCurso}`);
            setDataCourse(dataCourseResponse.data[0]);

            if (dataCourse && dataCourse.fecha && userEmail) {
                await checkUserIsSubscribed();

                if ((currentDate <= new Date(dataCourse.fecha))) {
                    setCourseDateIsAvailable(true);
                    await getDataInstructor(dataCourse.instructor);
                    
                }
            }
            } catch (error) {
            console.log(error);
            }
        }

        getDataCourse();
    }, [dataCourse.fecha, userIsSubscribed]);
    
  return (
    <section className='w-full flex relative bg-principal'>
        <AsideNav></AsideNav>
        <section className='h-full w-[83%] flex flex-col gap-10 py-12 px-9'>
            <div className='w-full flex justify-between gap-10'>
                <SearchBar></SearchBar>
                <TooglesUser></TooglesUser>
            </div>

            <article className='h-full w-full flex flex-col items-center gap-10'>
                <Toaster position='bottom-right'></Toaster>
                
                {
                    //? Si la fecha del curso es mayor a la fecha actual o el usuario está suscrito, se muestra la información del curso
                    (courseDateIsAvailable || userIsSubscribed) ? (
                        <section className='flex flex-col gap-8 w-full'>
                            <article className='w-full flex flex-col gap-7 bg-white p-9 rounded-2xl'>
                                <section className='flex gap-6'>
                                    <article className='w-1/2 flex flex-col gap-6'>
                                        <div className='flex flex-col gap-4'>
                                            <h2 className='font-helvetica-bold text-3xl text-primary'>{dataCourse.titulo}</h2>
                                            <span className='font-helvetica-medium text-lg text-text-accent'>Imparte: 
                                            { getInstructorName(dataInstructor) }</span>
                                        </div>

                                        <div>
                                            <h4 className='font-helvetica-bold text-lg text-text-primary'>Descripción</h4>
                                            <p className='text-text-accent'>
                                                {dataCourse.descripcion}
                                            </p>
                                        </div>

                                        <div>
                                            <h4 className='font-helvetica-bold text-lg text-text-primary'>Capacidad</h4>
                                            <span className='text-text-accent'>{dataCourse.capacidad} participantes (49 lugares restantes)</span>
                                        </div>

                                        {
                                            //? Si el usuario está suscrito, se muestra el enlace del curso
                                            (userIsSubscribed) && 
                                            <div>
                                                <h4 className='font-helvetica-bold text-lg text-text-primary'>{dataCourse.modalidad === 'Virtual'
                                                 ? "Enlace del curso" : "Inmueble de curso"}</h4>
                                                <span className='text-text-accent'>{dataCourse.lugarcurso !== null ? dataCourse.lugarcurso 
                                                : "Por definir..."}</span>
                                            </div>
                                        }

                                    </article>

                                    <article className='w-1/2 flex flex-col gap-6'>
                                        <div className='flex justify-between'>
                                            <section className='flex flex-col min-w-[25%] gap-6 bg-accent p-6 rounded-xl border'>
                                                <i className="ti ti-clock-hour-4 text-sat text-3xl"/>
                                                <div className='flex flex-col'>
                                                    <span className='text-sm text-text-primary'>Duración</span>
                                                    <span className='font-helvetica-bold text-text-accent text-xl'>{dataCourse.duracion} hr</span>
                                                </div>
                                            </section>

                                            <section className='flex flex-col min-w-[25%] gap-6 bg-accent p-6 rounded-xl border'>
                                                <i className="ti ti-users text-sat text-3xl"/>
                                                <div className='flex flex-col'>
                                                    <span className='text-sm text-text-primary'>Participantes</span>
                                                    <span className='font-helvetica-bold text-text-accent text-xl'>1</span>
                                                </div>
                                            </section>

                                            <section className='flex flex-col min-w-[25%] gap-6 bg-accent p-6 rounded-xl border'>
                                                <i className="ti ti-chalkboard text-sat text-3xl"/>
                                                <div className='flex flex-col'>
                                                    <span className='text-sm text-text-primary'>Modalidad</span>
                                                    <span className='font-helvetica-bold text-text-accent text-xl'>{dataCourse.modalidad}</span>
                                                </div>
                                            </section>
                                        </div>
                                            
                                        <div>
                                            <section className='w-full flex justify-start gap-16 bg-accent px-6 py-8 rounded-xl relative border'>
                                                <i className="ti ti-calendar text-sat text-3xl absolute top-0 right-0 m-4"></i>
                                                <div className='flex flex-col gap-1'>
                                                    <span className='text-sm text-text-primary'>Fecha:</span>
                                                    <span className='font-helvetica-bold text-text-accent text-xl'>{dataCourse.fecha ? destructureDate(dataCourse.fecha, true) : "Próximamente..."}</span>
                                                </div>

                                                <div className='flex flex-col gap-1'>
                                                    <span className='text-sm text-text-primary'>Horario:</span>
                                                    <span className='font-helvetica-bold text-text-accent text-xl'>{dataCourse.fecha ? `${destructureHours(dataCourse.fecha)} hrs` : "Próximamente..."}</span>
                                                </div>
                                            </section>
                                        </div>
                                            
                                        <div className='flex justify-between gap-4'>
                                            {
                                                userIsSubscribed ? (
                                                    <button className='bg-red-700 text-white font-helvetica-bold w-[50%] py-2 rounded-md'>Cancelar suscripcion</button>
                                                ) 
                                                : (
                                                    <div className='w-full flex gap-8'>
                                                        <button onClick={subscribeCourse} className='bg-sat text-white font-helvetica-bold w-full py-2 rounded-md'>Incribirme ahora</button>
                                                        
                                                    </div>
                                                )
                                            } 
                                        </div>
                                    </article>
                                </section>
                            </article>

                            {
                                (userIsSubscribed && currentDate >= new Date(dataCourse.fecha)) &&  
                                <section className='flex flex-col gap-8'>
                                    <article className='flex flex-col gap-7'>
                                        <h3 className='font-helvetica-bold text-2xl text-sat'>Tu opinión es importante</h3>
                                        <CourseSurvey courseID={idCurso}></CourseSurvey>
                                    </article>

                                    <article className='flex flex-col gap-7'>
                                        <h3 className='font-helvetica-bold text-2xl text-sat'>Comunidad del curso</h3>
                                        <CourseComments idCourse={idCurso}></CourseComments>
                                    </article>
                                </section>
                            }

                            {
                                (userIsSubscribed && currentDate < new Date(dataCourse.fecha)) &&
                                <SectionWaiting></SectionWaiting>
                                
                            }

                            {
                                (userIsSubscribed === false) && 
                                <SuscriptionPage></SuscriptionPage>
                            }
                    
                        </section>
                    )
                    :
                    (courseDateIsAvailable !== false || courseDateIsAvailable === null) ? (
                        <l-tailspin size="30" stroke="2" speed="1" color="#00529E"></l-tailspin>
                    ) : (
                        <span>El curso no está disponible</span>
                    )                    
                }
            </article>

        </section>
      
    </section>
  )
}

export default CourseSubscription