import React, { useState, useEffect } from 'react'
import { motion } from "framer-motion";
import axios from "axios";
import { toast } from "react-hot-toast";
import { useUserData } from "../../stores/user.data.store";
import { searchDataInstructor, getInstructorName } from "../../utilities/utilities";

function CourseComments({ idCourse }) {
    const [inputValue, setInputValue] = useState('');
    const [inputHeight, setInputHeight] = useState('15px');
    const [comments, setComments] = useState([]);
    const userEmail = useUserData((state) => state.userEmail);
    const [isResponse, setIsResponse] = useState(false);
    const [idCommentResponse, setIdCommentResponse] = useState(null);

    useEffect(() => {
        //* Aplicamos el efecto de crecimiento del textarea al ecsribir
        setInputHeight(`${document.getElementById('chatInput').scrollHeight}px`);
    }, [inputValue]);

    const prepareResponse = (idComment) => {
        setIdCommentResponse(idComment);
        setIsResponse(true);
    }

    useEffect(() => {
        async function fetchData() {
            try {
                //* Obtener todos los comentarios y respuestas del curso
                const responseAllComments = await axios.get(`http://localhost:3000/cursos/comentarios/obtener/${idCourse}`);
                const responseAllResponses = await axios.get(`http://localhost:3000/cursos/comentarios/respuestas/obtener/${idCourse}`);
                
                //* Organizar las respuestas por comentario
                const responsesByComment = {};
                responseAllResponses.data.forEach(response => {
                    const { idcomentario } = response;
                    if (!responsesByComment[idcomentario]) {
                        responsesByComment[idcomentario] = [];
                    }
                    responsesByComment[idcomentario].push(response);
                });
    
                //* Agregar las respuestas correspondientes a cada comentario
                const commentsWithResponses = responseAllComments.data.map(comment => ({
                    ...comment,
                    respuestas: responsesByComment[comment.idcomentario] || [] //* Asignar las respuestas correspondientes al comentario
                }));

                setComments(commentsWithResponses);
            } catch (error) {
                toast.error("Ha ocurrido un error al cargar los comentarios");
            }
        }
    
        fetchData();
    }, [comments]);
    

    const handleInputChange = (e) => {
        setInputValue(e.target.value);
    };
      
    const sendComment = async () => {
        if (inputValue === ''){
            toast.error("No puedes enviar un comentario vacío");
            throw new Error
        }
        
        setInputValue('');
        try {
            if (isResponse) {
                const dbResponse = await axios.post('http://localhost:3000/cursos/respuestas/nueva-respuesta', 
                { userEmail: userEmail, idComment: idCommentResponse, txtResponse: inputValue, idCourse: idCourse });
                
                if (dbResponse.status === 200) {
                    setIsResponse(false);
                    toast.success("Respuesta agregada con éxito");
                }else{
                    throw new Error;
                }
            }else{
                const dbResponse = await axios.post('http://localhost:3000/cursos/comentarios/nuevo-comentario', 
                { userEmail: userEmail, idCourse: idCourse, txtComment: inputValue });

                if (dbResponse.status === 200) {
                    comments.sort();
                    toast.success("Comentario agregado con éxito");
                }else{
                    throw new Error;
                }
            }  
        } catch (error) {
            toast.error("Ha ocurrido un error, intenta de nuevo");
        }
    }

    const scaleVariants = {
        hover: {
          scale: [1, 1.1, 1],
          transition: {
            duration: 0.4,
            repeat: 2,
            repeatType: "reverse"
          }
        }
      };

  return (
    <section className='flex'>
        <article className='flex flex-col gap-12 w-8/12 rounded-2xl p-10 bg-white relative'>
            <h2 className='text-text-accent font-helvetica-bold text-xl'>Preguntas, sugerencias y comentarios del curso</h2>
                {
                    comments.map(comment => (
                        <section key={comment.idcomentario} className='flex flex-col gap-2 mb-[-20px]'>

                            <section key={comment.idcomentario} className='rounded-lg border p-4 relative'>
                                <div className='flex items-center gap-2 mb-2'>
                                    <img className='h-8 rounded-full' src="https://udgfvlqwkrriakrdeehq.supabase.co/storage/v1/object/public/assets/386344463_625065573122411_5526941211183616566_n.jpg" alt="" />
                                    <span className='font-helvetica-medium text-text-accent text-sm'>{ comment.correo }</span>
                                </div>

                                <div>
                                    <p className='font-helvetica-light'>
                                        { comment.txtcomentario }
                                    </p>
                                </div>

                                <span className='absolute top-0 right-0 mx-6 my-2 font-helvetica-light text-sm'>15 h</span>

                                <span className='absolute bottom-0 right-0 mx-2 my-1 font-helvetica-light text-sm hover:cursor-pointer hover:text-sat-hover' onClick={() => prepareResponse(comment.idcomentario)}>Responder</span>

                            </section>
                            
                            {comment.respuestas.map(response => (
                                <section key={response.idrespuesta} className='ml-14 rounded-lg border p-4 relative bg-sat'>
                                    <div className='flex items-center gap-2 mb-2'>
                                        <img className='h-8 rounded-full' src="https://udgfvlqwkrriakrdeehq.supabase.co/storage/v1/object/public/assets/386344463_625065573122411_5526941211183616566_n.jpg" alt="" />
                                        <span className='font-helvetica-medium text-white text-sm'>{response.correo}</span>
                                    </div>

                                    <div>
                                        <p className='font-helvetica-light text-white'>
                                            {response.txtrespuesta}
                                        </p>
                                    </div>

                                    <span className='absolute top-0 right-0 mx-6 my-2 font-helvetica-light text-sm text-white'>15 h</span>
                                 </section>
                            ))}
                        </section>
                    ))
                }

            <article className='flex items-center gap-4 p-2 rounded-lg border relative'>
                <img className='h-8 rounded-full' src="https://udgfvlqwkrriakrdeehq.supabase.co/storage/v1/object/public/assets/386344463_625065573122411_5526941211183616566_n.jpg" alt="" />
                <textarea
                    className=" flex items-center w-full overflow-y-hidden resize-none border rounded-md px-3 py-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    id="chatInput"
                    placeholder={isResponse ? `Responder a Álvaro Isaías Espíndola Torres${getInstructorName(searchDataInstructor(comments, idCommentResponse))}` : "Escribe un comentario"}
                    value={inputValue}
                    style={{ height: inputHeight }}
                    onChange={handleInputChange}></textarea>

                <motion.i onClick={sendComment} className="ti ti-send text-sat text-2xl hover:cursor-pointer" whileHover="hover" variants={scaleVariants}></motion.i>
            </article>
            
        </article>         
    </section>
  )
}

export default CourseComments