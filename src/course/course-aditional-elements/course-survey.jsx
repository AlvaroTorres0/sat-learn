import React,{ useState, useEffect } from 'react'
import imageSurvey from "../../assets/mobile-in-hand.png";
import { supabaseClient } from "../../supabase/supabaseClient";
import { useUserData } from "../../stores/user.data.store";
import axios from "axios";
import toat, { Toaster } from "react-hot-toast";

function CourseSurvey({ courseID }) {
    const [questions, setQuestions] = useState([]);
    const [currentIndex, setCurrentIndex] = useState(0);
    const [userResponses, setUserResponses] = useState([]);
    const userEmail = useUserData((state) => state.userEmail);

    useEffect(() => {
        //* Obtener las preguntas de la encuesta	
        const getQuestions = async () => {
          try {
            const { data: questionsDataBase, error } = await supabaseClient
                .from("encuestas")
                .select("*");

            if (questionsDataBase) setQuestions(questionsDataBase);
            
            if (error) throw error;
          } catch (err) {
            console.log(err);
          }
        };

        //* Validar si el usuario ya ha respondido la encuesta
        const validateSurvey = async () => {
          const response = await axios.get(`http://localhost:3000/cursos/encuestas/validatesurvey/${courseID}/${userEmail}`);
          console.log(response.data.total_respuestas);
          if (response.data.total_respuestas > 0) {
            toat.success("Ya has respondido esta encuesta");
          }else{
            getQuestions();
          }
        }
        
        validateSurvey();
    },[]);

    const handleNext = () => {
        //* Avanzar al siguiente par de preguntas (si hay más preguntas disponibles)
        if (currentIndex + 2 < questions.length) {
          setCurrentIndex(prevIndex => prevIndex + 2);
          setUserResponses(prevResponses => {
            const updatedResponses = [...prevResponses];
            updatedResponses.push(null, null); //* Añadir valores nulos para las nuevas preguntas
            return updatedResponses;
          });
        } else {
          setCurrentIndex(0);
          setUserResponses(Array(questions.length).fill(null));
        }
      };
      
      const handleResponseChange = (response, questionIndex) => {
        setUserResponses(prevResponses => {
          const updatedResponses = [...prevResponses];
          updatedResponses[currentIndex + questionIndex] = parseInt(response);
          return updatedResponses;
        });
      };

      const submitResponses = async () => {
        const response = await axios.post('http://localhost:3000/cursos/encuestas', {
            courseID: courseID,
            userEmail: userEmail,
            surveyResponses: userResponses
        });
        if (response.status === 200) {
            toat.success("¡Gracias por responder la encuesta!");
        }
      }

      const allQuestionsAnswered = () => {
        //* Verificar si las preguntas dentro del rango actual (de dos en dos) tienen respuestas
        for (let i = currentIndex; i < currentIndex + 2; i++) {
          if (typeof userResponses[i] === 'undefined' || userResponses[i] === null) {
            return false; //* Devolver falso si alguna pregunta no tiene respuesta
          }
        }
        return true;
    }     
    

  return (
    <section className='w-full'>
        <Toaster position='bottom-right'></Toaster>
        <div className='flex justify-between'>
        {questions.length > 0 && (
            <article className='flex flex-col gap-12 w-8/12 rounded-2xl p-10 bg-white relative'>
            {/* Indicador de la sección de preguntas actual*/}
            <span className='absolute top-0 right-0 my-2 mx-4'>{`${currentIndex+1} / ${questions.length-1}`}</span>
            {/* Muestra las preguntas actuales */}
            {questions.slice(currentIndex, currentIndex + 2).map((question, index) => (
                <section key={question.IDPREGUNTA} className='flex flex-col gap-3'>
                    <div className='flex items-center gap-4 relative'>
                        <div className='grid place-items-center rounded-full w-8 h-8 absolute bg-sat text-white font-helvetica-bold'>
                          {currentIndex + index + 1}
                        </div>
                        <span className='font-helvetica-bold ml-12'>{question.txtpregunta}</span>
                    </div>

                    {/* Renderizar las opciones estáticas */}
                    <div className='flex gap-6 ml-12'>
                        <div className='flex gap-2'>
                            <input
                            type="radio"
                            name={`pregunta${currentIndex + index}`}
                            id={`pregunta-${currentIndex + index}-1`}
                            value={1}
                            checked={userResponses[currentIndex + index] === 1}
                            onChange={(e) => handleResponseChange(e.target.value, index)}
                            />
                            <span>1</span>
                        </div>

                        <div className='flex gap-2'>
                            <input
                            type="radio"
                            name={`pregunta${currentIndex + index}`}
                            id={`pregunta-${currentIndex + index}-2`}
                            value={2}
                            checked={userResponses[currentIndex + index] === 2}
                            onChange={(e) => handleResponseChange(e.target.value, index)}
                            />
                            <span>2</span>
                        </div>
                        <div className='flex gap-2'>
                        <input
                            type="radio"
                            name={`pregunta${currentIndex + index}`}
                            id={`pregunta-${currentIndex + index}-3`}
                            value={3}
                            checked={userResponses[currentIndex + index] === 3}
                            onChange={(e) => handleResponseChange(e.target.value, index)}
                            />
                            <span>3</span>
                        </div>
                        <div className='flex gap-2'>
                        <input
                            type="radio"
                            name={`pregunta${currentIndex + index}`}
                            id={`pregunta-${currentIndex + index}-4`}
                            value={4}
                            checked={userResponses[currentIndex + index] === 4}
                            onChange={(e) => handleResponseChange(e.target.value, index)}
                            />
                            <span>4</span>
                        </div>
                        <div className='flex gap-2'>
                        <input
                            type="radio"
                            name={`pregunta${currentIndex + index}`}
                            id={`pregunta-${currentIndex + index}-5`}
                            value={5}
                            checked={userResponses[currentIndex + index] === 5}
                            onChange={(e) => handleResponseChange(e.target.value, index)}
                            />
                            <span>5</span>
                        </div>
                    </div>
                </section>
))}

                {currentIndex + 2 < questions.length ? (
                    <button
                        className='bg-sat absolute text-white font-helvetica-bold px-6 py-1 rounded-lg bottom-0 right-0 mx-10 my-8'
                        onClick={handleNext}
                        disabled={!allQuestionsAnswered()} //* Deshabilitar el botón si no todas las preguntas están respondidas
                    >
                        Siguiente
                    </button>
                    ) : (
                    <button
                        className='bg-sat absolute text-white font-helvetica-bold px-6 py-1 rounded-lg bottom-0 right-0 m-10'
                        onClick={submitResponses}//* Deshabilitar el botón si no todas las preguntas están respondidas
                    >
                        Enviar respuestas
                    </button>
                    )}
            </article>
        )}

            <article className='flex flex-col w-4/12 items-center justify-start p-8'>
                <p className='text-center p-6'>
                    <span className='font-helvetica-bold'>Esta encuesta</span>, nos ayuda a medir el <span className='font-helvetica-bold'>desempeño</span> del instructor y del curso en general
                </p>
                <img src={imageSurvey} className='h-[150px]' alt="" />
                
            </article>

            
        </div>
    </section>
  )
}

export default CourseSurvey