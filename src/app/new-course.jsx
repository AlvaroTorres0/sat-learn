import { React, useState } from 'react'
import { FileDrop } from 'react-file-drop';
import { toast, Toaster } from "react-hot-toast";
import { supabaseClient } from "../supabase/supabaseClient.js";
import axios from "axios";
import { v4 } from "uuid";

function NewCourse() {
  const [formSection, setFormSection] = useState(1);
  const [imageCourse, setImageCourse] = useState(null);
  const [admonsGrales, setAdmonsGrales] = useState([]);
  const [presencial, setPresencial] = useState(true);
  const [dataCourse, setDataCourse] = useState({
    idCurso: v4(),
    nombreCurso: '',
    correoInstructor: '',
    descripcion: '',
    duracion: '',
    capacidad: '',
    administraciones: [],
    fechaHora: '',
    modalidad: 'Presencial',
    lugar: '',

  });

  const handleChangeDataCourse = (e) => {
    const { name, value } = e.target;
    setDataCourse(prevState => ({
      ...prevState,
      [name]: value
    }));
  };
  
  const handleNextSection = async () => {
    if (formSection === 1) {
      const isValidInstructor = await validateInstructor(dataCourse.correoInstructor);
      const dataIsValid = dataCourse.nombreCurso !== '' && dataCourse.correoInstructor !== '' && dataCourse.descripcion !== '';
    
      if (dataIsValid && isValidInstructor) {
        console.log(dataCourse);
        setFormSection(prevState => prevState + 1);
        setAdmonsGrales(await getAdministracionesGenerales());
      } else if (!dataIsValid) {
        toast.error("Revisa todos los campos");
      } else if (!isValidInstructor) {
        toast.error("No existe un instructor con ese correo");
      }
    }
    
    
    if (formSection === 2) {
      const isValidDuration = validateDuration();
      const isValidCourseCapacity = validateCourseCapacity();
    
      if (!isValidDuration) {
        toast.error("Ingresa una duración válida");
      } else if (!isValidCourseCapacity) {
        toast.error("Ingresa una capacidad válida");
      } else if (dataCourse.administraciones.length === 0) {
        toast.error("Selecciona al menos una administración");
      } else {
        setFormSection(prevState => prevState + 1);
      }
    }

    if (formSection === 3) {
      const isValidDate = validateDate();
      if (isValidDate) {
        if (dataCourse.modalidad === 'Virtual') {
          const isValidWebexLink = validateWebexLink();
          if(isValidWebexLink){
            handleCreateCourse();
          }else{
            toast.error("Ingresa un link de webex valido");
          }
        } 
        else if (dataCourse.modalidad === 'Presencial') {
          handleCreateCourse();
        }          
      }else{
        toast.error("Ingresa una fecha valida");
      }
    }
    
}

  const validateDate = () => {
    if (dataCourse.fechaHora === '') return true;
    
    const selectedDate = new Date(dataCourse.fechaHora);
    const currentDate = new Date();
    const selectedDayOfWeek = selectedDate.getDay();
    const selectedHour = selectedDate.getHours();

    const isValidDay = selectedDayOfWeek !== 0 && selectedDayOfWeek !== 6;
    const isValidHour = selectedHour > 9 || selectedHour < 16;
    const isValidDate = selectedDate >= currentDate;

    if (isValidDay && isValidHour && isValidDate) return true;
  }

  const validateCourseCapacity = () => {
    const capacity = Number(dataCourse.capacidad);
    return (capacity >= 10 && capacity <= 50);
  }

  const validateWebexLink = () => {
    const regexWebexLink = /^https:\/\/\w+\.webex\.com\/(?:meet|join)\/[\w-]+$/;
    return regexWebexLink.test(dataCourse.lugar);
  }

  async function validateInstructor(emailInstructor){
    try {
      const {data, error} = await supabaseClient
        .from('asignacion_roles')
        .select('*')
        .eq('correo', emailInstructor)
        .eq('idrol', '2');
        
        return data.length > 0;
    } catch (error) {
      return false;
    }
  }

  //* Función para validar la duración del curso
  const validateDuration = () => {
    const duration = dataCourse.duracion;
    return (/^(0[0-5]:[3-5][0-9]|06:00)$/.test(duration) || duration === '');
  };

  //* Función para obtener las administraciones generales
  const getAdministracionesGenerales = async () => {
    try {
      const { data, error } = await supabaseClient
      .from('admonsgrales')
      .select('*');

      if (error) {
        throw new Error(error.message);
      }else{
        return data;
      }
    } catch (error) {
      
    } 
  }

  //* Función para agregar o quitar administraciones a las cuales se agregará el curso
  const appendAdministraciones = (admin) => {
    const index = dataCourse.administraciones.indexOf(admin);
  
    if (index === -1) {
      setDataCourse(prevState => ({
        ...prevState,
        administraciones: [...prevState.administraciones, admin]
      }));
    } else {
      setAdmonsSelected(prevState => prevState.filter(admon => admon !== admin));
    }    
  }

  const handleCreateCourse = async () =>{
    
    try {
      const url = await uploadImageCourse(dataCourse.idCurso);
      const response = await axios.post('http://localhost:3000/nuevo/curso', {
        dataCourse: dataCourse,
        urlImageCourse: url.path
      });
      if(response.status === 200){
        toast.success("Curso creado");
      }
    }catch(err){
      toast.error("Error al crear el curso");
    }
  }
  
  const handleModalidadCourse = (modalidad) =>{
    if(modalidad === 'Presencial'){
      setPresencial(true);
      setDataCourse(prevState => ({
        ...prevState,
        modalidad: 'Presencial'
      }));
    }else{
      setPresencial(false);
      setDataCourse(prevState => ({
        ...prevState,
        modalidad: 'Virtual'
      }));
    }
  }

  async function uploadImageCourse(nameImage) {
    
    try {
      const { data, error } = await supabaseClient
      .storage
      .from('imagesCourses')
      .upload(`${nameImage}.jpeg`, imageCourse, {
        cacheControl: '3600',
        upsert: false
      }, {
        headers: {
          'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InVkZ2Z2bHF3a3JyaWFrcmRlZWhxIiwicm9sZSI6ImFub24iLCJpYXQiOjE2OTk0NzIyNjksImV4cCI6MjAxNTA0ODI2OX0.Kc0MfthL5zDvglo-xfVqtKkqhcksCziuOaUCdY2AT7c`
        }
      });
      if (data){
        return data
      };
      if (error) throw new Error(error.message);
    } catch (error) {
      toast.error("Error al cargar la imagen");
    }
  }



  async function loadImage(file){
    setImageCourse(file);
    toast.success("Imagen cargada");

    // Crear una URL local para la vista previa de la imagen
    const reader = new FileReader();
    reader.onload = (e) => {
      const previewURL = e.target.result;
      // Mostrar la vista previa de la imagen
      document.getElementById('image-preview').src = previewURL;
    };
    reader.readAsDataURL(file);
  }

  const renderSection = () => {
    switch (formSection) {
      case 1:
        return (
          <article className='flex flex-col gap-2'>
            <h4 className='font-helvetica-bold text-3xl'>Datos generales</h4>

            <section className='flex flex-col gap-8'>
              <article className='flex flex-col'>
                <label className='font-helvetica-medium text-lg'>Nombre del curso</label>
                <input type="text" required className='border rounded-md outline-none px-4 py-2' name='nombreCurso' 
                onChange={handleChangeDataCourse} value={dataCourse?.nombreCurso}/>
              </article>

              <article className='flex flex-col'>
                <label className='font-helvetica-medium text-lg'>Correo del instructor</label>
                <input type="email" required className='border rounded-md outline-none px-4 py-2' name='correoInstructor' 
                onChange={handleChangeDataCourse} value={dataCourse?.correoInstructor} />
              </article>

              <article className='flex flex-col'>
                <label className='font-helvetica-medium text-lg'>Descripción del curso</label>
                <textarea name="descripcion" required id="" cols="30" rows="4" className='px-4 py-2 border outline-none rounded-md resize-none' 
                onChange={handleChangeDataCourse} value={dataCourse?.descripcion}></textarea>
              </article>

              <div className='border border-dashed rounded-md'>
                <FileDrop
                    onDrop={(files, event) => loadImage(files[0])} className='h-[100px]'>
                      <div className='h-[100px]'>
                        {
                          imageCourse !== null ? (
                            <div className='h-full flex flex-col items-center justify-center'>
                              <img
                              id='image-preview'
                              className='h-[80px] rounded-md object-cover'
                              src={imageCourse ? URL.createObjectURL(imageCourse) : ''}
                              alt='Vista previa'
                              />
                            </div>
                          ) : (
                            <div className='flex flex-col items-center justify-center'>
                              <i className="ti ti-upload block text-3xl"></i>
                              <span className='font-helvetica-medium'>Arrastra y suelta una imagen para el curso (opcional)</span>
                              <span className='text-sm'>Peso max. 4MB .jpg .png .jpeg</span>
                            </div>
                          )
                        }
                        
                      </div>
                </FileDrop>
              </div>
            </section>
          </article>
        );
      case 2:
        return (
          <article className='flex flex-col gap-2'>
            <h4 className='font-helvetica-bold text-3xl'>Datos complementarios</h4>

            <section className='flex flex-col gap-8'>
              <article className='flex flex-col'>
                <label className='font-helvetica-medium text-lg'>Duración del curso (min 00:30 minutos, max 06:00)</label>
                <input type="text" required className='border rounded-md outline-none px-4 py-2' placeholder='Ejemplo: 01:30' name='duracion' onChange={handleChangeDataCourse} value={dataCourse?.duracion}/>
              </article>

              <article className='flex flex-col'>
                <label className='font-helvetica-medium text-lg'>Capacidad del curso (max. 50 personas)</label>
                <input type="text" required className='border rounded-md outline-none px-4 py-2' name='capacidad' onChange={handleChangeDataCourse} value={dataCourse?.capacidad} />
              </article>

              <article className='flex flex-col'>
                <label className='font-helvetica-medium text-lg'>Seleccione las administraciones a las que se dirige</label>
                <div className='flex flex-wrap gap-4'>
                  {
                    admonsGrales.map((admin) => {
                      return(
                        <div key={admin?.idadmongral} onClick={() => appendAdministraciones(admin?.idadmongral)} className={(dataCourse.administraciones.includes(admin?.idadmongral)) ? 'grid place-items-center rounded-lg bg-[#F3FFF3] w-20 py-3 hover:cursor-pointer' : 'grid place-items-center rounded-lg bg-accent w-20 py-3 hover:cursor-pointer'}>
                          <span className='font-helvetica-light'>{admin?.idadmongral}</span>
                        </div>
                      )
                    })

                  }

                </div>
              </article>

            </section>
          </article>
        );
        case 3:
          return (
            <article className='flex flex-col gap-2'>
            <h4 className='font-helvetica-bold text-3xl'>Modalidad</h4>

            <section className='flex flex-col gap-8'>
              <div className='flex flex-col'>
                <label className='font-helvetica-medium text-lg'>Fecha y hora del curso (opcional)</label>
                <input type="datetime-local" className='border rounded-md outline-none px-4 py-2' name='fechaHora' onChange={handleChangeDataCourse} value={dataCourse?.fechaHora}/>
              </div>

              <div className='flex flex-col'>
                <label className='font-helvetica-medium text-lg'>Modalidad</label>
                <div className='flex flex-col gap-2'>
                  <div className={presencial ? 'bg-[#F3FFF3] rounded-lg p-4 hover:cursor-pointer' : 'bg-accent rounded-lg p-4 hover:cursor-pointer'} onClick={() => handleModalidadCourse('Presencial')}>
                    <div>
                      <span className='font-helvetica-medium text-lg'>Presencial</span>
                      <p className='text-xs'>En el siguiente campo podrás definir las instalaciones y el inmueble o también puedes definirlas después</p>
                    </div>
                  </div>

                  <div className={presencial ? 'bg-accent rounded-lg p-4 hover:cursor-pointer' : 'bg-[#F3FFF3] rounded-lg p-4 hover:cursor-pointer'} onClick={() => handleModalidadCourse('Virtual')}>
                    <div>
                      <span className='font-helvetica-medium text-lg'>Virtual</span>
                      <p className='text-xs'>En el siguiente campo podrás definir un enlace para la video llamada o también puedes definirlas después</p>
                    </div>
                  </div>
                </div>
              </div>

              <div className='flex flex-col'>
                <label className='font-helvetica-medium text-lg'>Lugar del curso (opcional)</label>
                <input type="text" className='border rounded-md outline-none px-4 py-2' name='lugar' onChange={handleChangeDataCourse} value={dataCourse?.lugar}/>
              </div>

            </section>
          </article>
          )

      default:
        return null;
    }
  };

  return (
    <section className='relative bg-principal'>
      <Toaster position='bottom-right'></Toaster>
      <article className='w-[80vw] flex flex-col items-center px-9 gap-10'>
        <div className='bg-white flex flex-col gap-6 w-[50%] h-[80vh] rounded-[30px] p-14'>
          <h3 className='font-helvetica-bold text-5xl'>Nuevo curso</h3>
          {renderSection()}

          {
            formSection !== 3 && (
              <button onClick={handleNextSection} className='bg-sat text-white font-helvetica-bold rounded-lg px-8 py-2'>Siguiente</button>
            ) 
            ||
            formSection === 3 && (
              <button onClick={handleNextSection} className='bg-sat text-white font-helvetica-bold rounded-lg px-8 py-2'>Publicar curso</button>
            )
          } 
        </div>
      </article>
    </section>
  )
}

export default NewCourse