import React from 'react';
import { useSearchResults } from '../stores/app.searchResults.store';
import CardCursoPopular from "../cards-cursos/card-curso-popular";

function DiscoverMore() {
  const searchResults = useSearchResults((state) => state.searchResults);

  return (
    <section className='w-full flex gap-10 flex-wrap'>
      {
        searchResults.map(curso => {
          const {idcurso, instructor, fecha, titulo, descripcion, imagenCurso} = curso;
          return(
            <CardCursoPopular key={idcurso} idCurso={idcurso} instructor={instructor} fecha={fecha} titulo={titulo} 
            descripcion={descripcion} imagenCurso={imagenCurso}></CardCursoPopular>
          )
        })
      }

      
    </section>
  );
}

export default DiscoverMore;
