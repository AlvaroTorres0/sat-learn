import React, { useEffect, useState } from 'react'
import './animations.css'
import axios from "axios";
import { useUserData } from "../stores/user.data.store";
import CardCursoPopular from "../cards-cursos/card-curso-popular";
import CardCursoPendiente from "../cards-cursos/card-curso-pendiente";
import CardCursoProximamente from "../cards-cursos/card-curso-proximamente";
import CardCursoRecomendado from "../cards-cursos/card-curso-recomendado";

function principal() {
  const { userData } = useUserData();
  //* Estados para cada una de las secciones de la página principal
  const [cursosProximamente, setCursosProximamente] = useState([]);
  const [cursosPendientes, setCursosPendientes] = useState([]);
  const [cursosPopulares, setCursosPopulares] = useState([]);
  const [cursosRecientes, setCursosRecientes] = useState([]);
  const [cursosRecomendados, setCursosRecomendados] = useState([]);
  const [loader, setLoader] = useState(true);
  const classSections = loader ? 'section-loading flex flex-col gap-7 bg-white p-9 rounded-2xl' : 'flex flex-col gap-7 bg-white p-9 rounded-2xl';
  


  useEffect(() => {
    const getCursosProximos = async (admonGralUser) => {
      try {
        const response = await axios.get(`http://localhost:3000/cursos/proximamente/${admonGralUser}`);
        if (response.status === 200) setCursosProximamente(response.data);
        if (response.status === 500) throw new Error();
      } catch (error) {
        console.error(error);
      }
    };
  
    const getCursosPendientes = async (userEmail) => {
      try {
        const response = await axios.get(`http://localhost:3000/cursos/pendientes/${userEmail}`);
        if (response.status === 200) setCursosPendientes(response.data);
        if (response.status === 500) throw new Error();
      } catch (error) {
        console.error(error);
      }
    };
  
    const getCursosPopulares = async (admonGralUser) => {
      try {
        const popularCourses = await axios.get(`http://localhost:3000/cursos/populares/${admonGralUser}`);
        if (popularCourses.status === 200) setCursosPopulares(popularCourses.data);
      } catch (err) {
        console.error(err);
      }
    };
    const getCursosRecomendados = async (userEmail,admonGralUser) => {
      try {
        const recommendedCourses = await axios.get(`http://localhost:3000/cursos/recomendados/${userEmail}/${admonGralUser}`);
        if (recommendedCourses.status === 200){
          console.log(recommendedCourses.data);
          setCursosRecomendados(recommendedCourses.data);
        }
      } catch (err) {
        console.error(err);
      }
    }
  
    const getCursosRecientes = async (admonGralUser) => {
      try {
        const recentCourses = await axios.get(`http://localhost:3000/cursos/recientes/${admonGralUser}`);
        if (recentCourses.status === 200) setCursosRecientes(recentCourses.data);
        if (recentCourses.status === 500) throw new Error();
      } catch (err) {
        console.error(err);
      }
    };
  
    const loadData = async () => {
      if (userData && userData.admongral) {
        const userEmail = userData.correo;
  
        try {
          //* Ejecutar todas las funciones asincrónicas simultáneamente
          await Promise.all([
            getCursosProximos(userData.admongral),
            getCursosRecomendados(userEmail,userData.admongral),
            getCursosPendientes(userEmail),
            getCursosPopulares(userData.admongral),
            getCursosRecientes(userData.admongral),
          ]);
  
          setLoader(false);
        } catch (error) {
          console.error(error);
        }
      }
    };
  
    loadData();
  }, [userData]);
  
  return (
    <>
              <article className='w-[50vw] flex flex-col gap-10'>
                <section className={classSections}>
                  <h3 className={loader ? 'hidden' : 'font-helvetica-bold text-2xl text-sat'}>Más populares</h3>
                  <article className='flex grow-0 justify-between flex-wrap gap-5'>
                    {
                      cursosPopulares.map(curso => {
                        const {idcurso, instructor, fecha, titulo, descripcion, imagenCurso} = curso;
                        return(
                          <CardCursoPopular key={idcurso} idCurso={idcurso} instructor={instructor} fecha={fecha} titulo={titulo} descripcion={descripcion} imagenCurso={imagenCurso}></CardCursoPopular>
                        )
                      })
                    }
                  </article>
                </section>

                <section className={classSections}>
                  <h3 className={loader ? 'hidden' : 'font-helvetica-bold text-2xl text-sat'}>Recomendados</h3>
                  <article className='flex justify-between flex-wrap gap-5'>
                    {
                      cursosRecomendados.map(curso => {
                        const {idcurso,titulo,fecha,imgcurso,instructor} = curso;
                        return(
                          <CardCursoRecomendado key={idcurso} idCurso={idcurso} titulo={titulo} fecha={fecha} imgCurso={imgcurso} correoInstructor={instructor}></CardCursoRecomendado>
                        )
                      })
                    }

                  </article>
                </section>

                <section className={classSections}>
                  <h3 className={loader ? 'hidden' : 'font-helvetica-bold text-2xl text-sat'}>Agregados recientemente</h3>
                  <article className='flex justify-between flex-wrap gap-5'>
                    {
                      cursosRecientes.map(curso => {
                        const {idcurso,titulo,fecha,imgcurso,instructor} = curso;
                        return(
                          <CardCursoRecomendado key={idcurso} idCurso={idcurso} titulo={titulo} fecha={fecha} imgCurso={imgcurso} correoInstructor={instructor}></CardCursoRecomendado>
                        )
                      })
                    }
                  </article>
                </section>
              </article>

              <article className='w-[30vw] flex flex-col pl-9 gap-10'>
                <section className={classSections}>
                  <h3 className={loader ? 'hidden' : 'font-helvetica-bold text-2xl text-sat'}>Tus cursos pendientes</h3>
                  <article className='flex justify-between flex-wrap gap-5'>
                    {
                      cursosPendientes.map(curso => {
                        const {idcurso,titulo,fecha,imgcurso} = curso;
                        
                        return(
                          <CardCursoPendiente key={idcurso} idCurso={idcurso} titulo={titulo} fecha={fecha} imgCurso={imgcurso}></CardCursoPendiente>
                        )
                      })
                    }
                    
                  </article>
                </section>

                <section className={classSections}>
                  <h3 className={loader ? 'hidden' : 'font-helvetica-bold text-2xl text-sat'}>Próximamente</h3>
                  <article className='flex justify-between flex-wrap gap-5'>
                    {
                      cursosProximamente.map(curso => {
                        const {idcurso,imgcurso,instructor,titulo} = curso;
                        return(
                          <CardCursoProximamente key={idcurso} idCurso={idcurso} urlImageCurso={imgcurso} correoInstructor={instructor} titulo={titulo}></CardCursoProximamente>
                        )

                      })
                    }
                  </article>
                </section>
              </article>
            </>
  )
}

export default principal