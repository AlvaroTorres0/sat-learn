import React from 'react'
import AsideNav from "../components/aside-nav/aside-nav";
import SearchBar from "../components/searchBar";
import TooglesUser from "../components/tooglesUser";
import { motion } from "framer-motion";
import { useActualPortalSection } from "../stores/app.section.store";
import Principal from "./principal";
import NewCourse from "./new-course";
import DiscoverMore from "./discover-more";

function Home() {
  const actualPortalSection = useActualPortalSection((state) => state.actualPortalSection);

  const renderAppSections = () => {
    switch (actualPortalSection) {
      case 1:
        return(
          <Principal></Principal>

        )

      case 2:
        return(
          <DiscoverMore></DiscoverMore>
        )

      case 5:
        return(
          <NewCourse></NewCourse>
        )
        
      default:

    }
  }
  
    
  return (
    <main className='flex relative bg-principal'>
      <AsideNav></AsideNav>
      <section className='h-full w-[83%] flex flex-col gap-10 py-12 px-9'>
        <div className='w-full flex justify-between gap-10'>
          <SearchBar></SearchBar>
          <TooglesUser></TooglesUser>
        </div>

        <motion.section className='w-full flex'>
          {
            renderAppSections()
          }
        </motion.section>



      </section>
    </main>
  )
}

export default Home