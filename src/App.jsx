import { Route, Routes, useNavigate } from "react-router-dom";
import Login from "./auth/login";
import { Signup } from "./auth/signup";
import Home from "./app/home";
import CourseSubscription from "./course/course-subscription";
import { useEffect } from "react";
import { supabaseClient } from "./supabase/supabaseClient";
import { useUserData } from "./stores/user.data.store";
function App() {

  //! Importamos los hooks de la store
  const { setUserRole, setUserData, setUserEmail } = useUserData();
  const routerNavigate = useNavigate();

    async function fetchUserRole(email){
      try {
        const { data: USER_ROLE, error} = await supabaseClient
          .from('asignacion_roles')
          .select('idrol')
          .eq('correo', email)

          if (error) throw new Error;

          setUserRole(USER_ROLE[0].idrol);
      } catch (error) {
        console.log(error);
      }
    }

    async function fetchUserData(email) {
      try {
        const { data: ALL_DATA_USER , error } = await supabaseClient
        .from('usuarios')
        .select('*')
        .eq('correo', email);

        if (error) throw new Error;
        setUserData(ALL_DATA_USER[0]);        
      } catch (error) {
        console.error('Error al obtener el usuario:', error);
      }
    }

    async function fetchUserEmail() {
      try {
        const { data: { user } } = await supabaseClient.auth.getUser();
        setUserEmail(user.email);
        return user.email;
        
      } catch (error) {
        console.error('Error al obtener el usuario:', error);
      }
    }

  useEffect(() => {

    const getUserSession = async () => {

      const { data, error } = await supabaseClient.auth.getSession()
      if (error) {
        throw new Error(error.message);
      }
      if (!data.session) {
        routerNavigate('/iniciar-sesion'); 
      }
      if (data) {
        const email = await fetchUserEmail();
        await Promise.all([
          fetchUserRole(email),
          fetchUserData(email),
        ])
        .catch((error) => {
          console.error('Error al obtener los datos del usuario:', error);
        })
        .finally(() => {
          console.log('Los datos del usuario han sido cargados');
        });
      }
    }

    getUserSession();
  }, []);


  

  return (
    <>
      <Routes>
        <Route path="/" element={<Home />}/>
        <Route path="/home" element={<Home />}/>
        <Route path="/iniciar-sesion" element={<Login />}/>
        <Route path="/registro" element={<Signup />}/>
        <Route path="/subscripcion/curso/:idCurso" element={<CourseSubscription />}/>
      </Routes>
    </>
  )
}

export default App
