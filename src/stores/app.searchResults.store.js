import { create } from "zustand";

export const useSearchResults = create((set) => ({
    searchResults: [],
    setSearchResults: (searchResults) => set({ searchResults }),
}));