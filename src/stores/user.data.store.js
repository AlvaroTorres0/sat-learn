import { create } from 'zustand'

export const useUserData = create((set) => ({
    userData: null,
    userRole: null,
    userEmail: null,
    setUserData: (userData) => set({ userData }),
    setUserRole: (userRole) => set({ userRole }),
    setUserEmail: (userEmail) => set({ userEmail }),
  }));
  