import { create } from "zustand";

export const useActualPortalSection = create((set) => ({
    actualPortalSection: 1,
    setActualPortalSection: (actualPortalSection) => set({ actualPortalSection }),
}));