import React,{ useEffect, useState } from 'react'
import avatar from "./avatar.jpg";
import { Link } from "react-router-dom";
import { searchDataInstructor, getInstructorName, destructureDate, destructureHours } from '../utilities/utilities';
import { motion } from "framer-motion";

function CardCursoRecomendado({ idCurso, titulo, fecha, imgCurso, correoInstructor }) {
    const [dataInstructor, setDataInstructor] = useState({});
    const [urlImgCurso, seturlImgCurso] = useState(`https://udgfvlqwkrriakrdeehq.supabase.co/storage/v1/object/public/imagesCourses/${imgCurso}`);

    useEffect(() => {
        const fetchData = async () => {
            const instructorData = await searchDataInstructor(correoInstructor);
            if (instructorData && instructorData !== null) {
                setDataInstructor(instructorData);
            }
            const container = document.querySelector(`.container-${idCurso}`);
            container.style.backgroundImage = `url(${urlImgCurso})`;
        };

        fetchData();
        
    }, [correoInstructor]);

  return (
    <article className={`container-${idCurso} bg-cover bg-no-repeat bg-center w-[85%] rounded-3xl desktop:w-full`}>
        <div className='bg-gradient-to-b from-[rgba(0,0,0,0.5)] to-[rgba(0,0,0,0.7)] w-full flex flex-col justify-center gap-4 rounded-3xl desktop:gap-5 desktop:px-10 desktop:py-5'>
            <section className='text-slate-100 flex justify-between w-full'>
                <div className='flex gap-6'>
                    <img src={avatar} alt="Tu imagen" className='rounded-full h-10' />
                    <div className='flex flex-col justify-between opacity-90'>
                        <h3 className='font-helvetica-bold text-base leading-none'>{getInstructorName(dataInstructor)}</h3>
                        <div className='flex gap-8'>
                            {
                                fecha && fecha !== null &&
                                <>
                                    <span className='font-helvetica-bold text-xs'>{destructureDate(fecha)}</span>
                                    <span className='font-helvetica-light text-xs'>{`${destructureHours(fecha, false)} hrs`}</span>
                                </>
                            }
                            
                        </div>
                    </div>
                </div>

                <motion.i className='ti ti-heart text-slate-200 text-3xl hover:cursor-pointer' whileHover={{ scale: 1.1, color: '#ff0000', transition: { duration: 0.4, ease: 'easeInOut', repeat: Infinity, repeatType: "reverse"} }} whileTap={{ scale: 0.9 }}></motion.i>

            </section>

            <section className='text-white flex justify-between items-center gap-4'>
                <div>
                    <h2 className='font-helvetica-bold text-2xl'>{titulo}</h2>   
                    <span className='text-sm text-green-500'>50 disponibles</span>
                </div>
                <Link to={`/subscripcion/curso/${idCurso}`}>
                    <motion.button className='font-helvetica-bold bg-sat px-10 py-2 rounded-lg tablet:py-1 desktop:py-1 duration-200 hover:bg-sat-hover'>Ver</motion.button>
                </Link>
            </section>
        </div>

    </article>
  )
}

export default CardCursoRecomendado