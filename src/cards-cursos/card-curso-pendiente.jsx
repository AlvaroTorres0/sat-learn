import React from 'react'
import { Link } from "react-router-dom";
import { destructureDate, destructureHours } from "../utilities/utilities";

function CardCursoPendiente({ idCurso, titulo, fecha, imgCurso }) {
  return (
    <article className='w-full border rounded-2xl flex items-center justify-between px-7 py-4 gap-5 
    bg-white shadow-[1px_7px_9px_-1px_rgba(0,0,0,0.1)]'>
        <section className='flex w-1/2 flex-col overflow-hidden'>
          <h2 className='font-helvetica-bold text-lg truncate'>{`${titulo}`}</h2>
          <div className='flex flex-col justify-between'>
            <span className='font-helvetica-bold text-sm'>{destructureDate(fecha,false)}</span>
            <span className='text-sm'>{destructureHours(fecha)} hrs</span>
          </div>
        </section>

        <button className='flex justify-around text-sm'>
            <Link to={`/subscripcion/curso/${idCurso}`} className='rounded-2xl text-sat font-helvetica-bold px-3 py-2 
            shadow-md tablet:px-4 tablet:text-sm'>Ver detalles</Link>
        </button>
    </article>
  )
}

export default CardCursoPendiente