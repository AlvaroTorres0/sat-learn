import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { searchDataInstructor, getInstructorName } from '../utilities/utilities';

function CardCursoProximamente({ idCurso, urlImageCurso, titulo, correoInstructor }) {
  const [dataInstructor, setDataInstructor] = useState({});

    useEffect(() => {
      const fetchData = async () => {
        const instructorData = await searchDataInstructor(correoInstructor);
        if (instructorData && instructorData !== null) {
          setDataInstructor(instructorData);
        }
      };

      fetchData();
    }, [correoInstructor]);
    
  return (
    <article className='h-[80px] gap-2 py-2 px-2 bg-white border rounded-2xl flex shadow-[1px_7px_9px_-1px_rgba(0,0,0,0.1)] tablet:h-[90px] desktop:w-[500px] desktop:h-[80px]'>
      <section className='grid place-items-center w-1/6'>
        <img className='w-[40px] object-contain' src={`https://udgfvlqwkrriakrdeehq.supabase.co/storage/v1/object/public/imagesCourses/${urlImageCurso}`} alt='Imagen del curso' />
      </section>

      <section className='flex flex-col justify-center text-xs w-4/6'>
        <h2 className='font-helvetica-bold'>{titulo}</h2>
        <span className='tablet:mb-1'>{getInstructorName(dataInstructor)}</span>
      </section>

      <section className='flex items-center justify-center text-xs w-1/6'>
        <Link to={`/subscripcion/curso/${idCurso}`} className='text-sat'>
          Reservar
        </Link>
      </section>
    </article>
  );
}

export default CardCursoProximamente;
