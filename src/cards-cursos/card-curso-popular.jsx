import React,{ useEffect, useState } from 'react'
import avatar from "./avatar.jpg";
import { Link } from "react-router-dom";
import { destructureDate, searchDataInstructor, getInstructorName, destructureHours } from "../utilities/utilities";
import { motion } from "framer-motion";

function CardCursoPopular({ idCurso, instructor, fecha, titulo, descripcion, imagenCurso }) {

    const [dataInstructor, setDataInstructor] = useState({});
    const [urlImgCurso, seturlImgCurso] = useState(`https://udgfvlqwkrriakrdeehq.supabase.co/storage/v1/object/public/imagesCourses/${idCurso}.jpeg`);

    useEffect(() => {
      const fetchData = async () => {
        const instructorData = await searchDataInstructor(instructor);
        if (instructorData && instructorData !== null) {
          setDataInstructor(instructorData);
        }
        const container = document.querySelector(`.container-${idCurso}`);
        container.style.backgroundImage = `url(${urlImgCurso})`;
        
      };

      fetchData();
    }, [instructor, imagenCurso]);
    
  return (
    <article className={`container-${idCurso} bg-cover bg-no-repeat bg-center w-[85%] h-[300px] rounded-3xl tablet:w-[80%] 
    desktop:w-[380px] desktop:h-[345px]`}>
        <div className='bg-gradient-to-b from-[rgba(0,0,0,0.5)] to-[rgba(0,0,0,0.8)] w-full h-full relative flex flex-col 
        justify-center px-5 gap-4 rounded-3xl desktop:gap-5 desktop:px-6'>
            <motion.i className='ti ti-heart absolute top-0 right-0 my-2 mx-4 text-slate-200 text-3xl hover:cursor-pointer' whileHover={{ scale: 1.1, color: '#ff0000', transition: { duration: 0.4, ease: 'easeInOut', repeat: Infinity, repeatType: "reverse"} }} whileTap={{ scale: 0.9 }}></motion.i>
            <section className='text-slate-100'>
                <div className='flex items-center gap-4'>
                    <img src={avatar} alt="Tu imagen" className='rounded-full w-10' />
                    <div className='flex flex-col gap-2 opacity-90'>
                        <h3 className='font-helvetica-bold text-white text-base leading-none'>{getInstructorName(dataInstructor)}</h3>
                        <div className='flex gap-6'>
                            <span className='font-helvetica-bold text-xs'>{destructureDate(fecha,false)}</span>
                            <span className='font-helvetica-light text-xs'>{`${destructureHours(fecha)} hrs`}</span>
                        </div>
                    </div>
                </div>

            </section>

            <section className='text-white flex flex-col gap-4'>
                <div>
                    <h2 className='font-helvetica-bold text-xl'>{titulo}</h2>   
                    <div className='flex flex-col'>
                        <span className='text-xs text-slate-200'>1 disponible</span>
                    </div>
                </div>
                <p className='font-helvetica-medium text-xs text-white line-clamp-2 tablet:text-sm desktop:line-clamp-3 '>
                    {descripcion}
                </p>
                <Link to={`/subscripcion/curso/${idCurso}`}>
                    <button className='font-helvetica-bold bg-sat px-10 py-2 w-full rounded-lg tablet:py-1 desktop:py-1 hover:bg-sat-hover'>
                        Ver más  
                    </button>
                </Link>
            </section>
        </div>

    </article>
  )
}

export default CardCursoPopular