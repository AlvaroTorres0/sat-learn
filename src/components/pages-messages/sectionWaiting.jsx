import React from 'react'
import calendar from "../../assets/calendar.png";

function SectionWaiting() {
  return (
    <section className='w-4/5 flex items-center'>
        <div>
            <h2 className='font-helvetica-light text-[20px]'>Una vez llegada la fecha podrás acceder al contenido completo</h2>
        </div>

        <div>
            <img className='w-[250px]' src={calendar} alt="" />
        </div>
    </section>
  )
}

export default SectionWaiting