import React from 'react'
import imgSuscribe from "../../assets/puzzle.png";

function SuscriptionPage() {
  return (
    <section className='w-4/5 flex items-center'>
        <div>
            <h2 className='font-helvetica-light text-[20px]'>Suscríbete para obtener el contenido completo del curso</h2>
        </div>

        <div>
            <img className='w-[250px]' src={imgSuscribe} alt="" />
        </div>
    </section>
  )
}

export default SuscriptionPage