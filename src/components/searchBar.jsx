import React,{ useState } from 'react'
import axios from "axios";
import { useUserData } from "../stores/user.data.store";
import { useSearchResults } from "../stores/app.searchResults.store";
import { useActualPortalSection } from "../stores/app.section.store";

function SearchBar() {
  const [searchValue, setSearchValue] = useState('');
  const userData = useUserData((state) => state.userData);
  const {setSearchResults} = useSearchResults();
  const {setActualPortalSection} = useActualPortalSection();

  const search = async () => {
    try {
      const response = await axios.get(`http://localhost:3000/buscar/cursos/${userData.admongral}/${searchValue}`);
      setSearchResults(response.data);
      setActualPortalSection(2);
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <article className='bg-white h-14 w-[62%] px-4 flex items-center relative rounded-full'>
        <i className="ti ti-search mx-5 text-2xl"></i>
        <input className='h-full w-2/3 bg-transparent outline-none' 
               type="text" 
               placeholder="Ingresa tu búsqueda"
               onChange={(e) => setSearchValue(e.target.value)} value={searchValue} />

        <button className='h-14 w-14 bg-sat rounded-full absolute right-0 hover:bg-sat-hover' onClick={search}>
            <i className="ti ti-arrow-narrow-right text-slate-100 text-2xl"></i>
        </button>
    </article>
  )
}

export default SearchBar