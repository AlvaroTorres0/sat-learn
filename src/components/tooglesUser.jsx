import { React, useState } from 'react'
import { useUserData } from "../stores/user.data.store";
import { motion } from "framer-motion";
import { supabaseClient } from "../supabase/supabaseClient";


function TooglesUser() {
    const urlImageProfile = useUserData((state) => state.userData?.urlimgusuario);

    async function closeSesion(){
        const {error} = await supabaseClient.auth.signOut();
        if (error) throw new Error;
    }

  return (
    <section className='flex items-center gap-10'>
        <motion.div 
               whileHover={{ color: "#00529E" }}
                className='h-6 flex items-center gap-2 font-helvetica-bold hover:cursor-pointer'>
                    <i className="ti ti-settings text-2xl"></i>
                    <span>Configuraciones</span>
            </motion.div>

            <motion.div 
                whileHover={{ color: "#FF0000" }}
                onClick={() => closeSesion()}
                className='h-6 flex items-center gap-2 font-helvetica-bold hover:cursor-pointer'>
                    <i className="ti ti-logout-2 text-2xl"></i>
                    <span>Cerrar sesión</span>
            </motion.div>

        <button className='z-20'>
            <img className='rounded-full w-14' src={urlImageProfile} alt=""/>
        </button>

    </section>
  )
}

export default TooglesUser