import React, { useState } from 'react'
import { useUserData } from "../../stores/user.data.store";
import { motion } from "framer-motion";
import { Link } from "react-router-dom";
import { useActualPortalSection } from "../../stores/app.section.store";

function Nav() {
  const {setActualPortalSection} = useActualPortalSection();
  const userRole = useUserData((state) => state.userRole);
  const [active, setActive] = useState(0);
  const activeVariants = {
    "0": {
      color: "#00529E",
    },
    "1": {
      color: "#00529E",
    },
    "2": {
      color: "#00529E",
    },
    "3": {
      color: "#00529E",
    },
    "4": {
      color: "#00529E",
    },
    "5": {
      color: "#00529E",
    }
    
  }

  const itemsnav = [
    {itemName : "Principal", icon: "ti ti-category-2",},
    {itemName : "Descubre más", icon: "ti ti-search"},
    {itemName : "Mis cursos", icon: "ti ti-list-details"},
    {itemName : "Reportar problema", icon: "ti ti-flag-exclamation"},
  ]

  const setActualPortalSectionHandler = (index) => {
    setActive(index);
    setActualPortalSection(index+1);
  }
  
  return (
      <ul className='flex flex-col gap-7 py-[26px] justify-between'>

        {
          itemsnav.map((item, index) => {
            const { itemName, icon, link } = item; 
            return(
              <motion.li key={itemName} whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }} onClick={() => setActualPortalSectionHandler(index)} 
              className='flex items-center gap-5 hover:cursor-pointer'>
                <motion.i animate={active === index ? `${index}` : "inactive"} variants={activeVariants} className={icon} style={{fontSize: "25px"}}></motion.i>
                <motion.span animate={active === index ? `${index}` : "inactive"} variants={activeVariants} className='font-helvetica-medium text-[17px]
                 text-slate-600'>{itemName}</motion.span>
              </motion.li>
            )
          })
        }

        {
          userRole === '1' && 
            <motion.li whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }} onClick={() => setActualPortalSectionHandler(4)}>
              <Link className='flex items-center gap-5 hover:cursor-pointer'>
                <motion.i animate={active === 5 ? `${5}` : "inactive"} className="ti ti-cube-plus" style={{fontSize: "25px"}}></motion.i>
                <motion.span animate={active === 5 ? `${5}` : "inactive"} className='font-helvetica-medium text-[17px] text-slate-600'>Nuevo curso</motion.span>
              </Link>
            </motion.li>
        }
      </ul>
  )
}

export default Nav