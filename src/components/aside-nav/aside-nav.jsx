import React from 'react';
import logoSat from "../../assets/logo-sat.png";
import Nav from "./nav";
import CardSat from "./card-stat/card-stat";
import { objectCardsStats } from "./card-stat/info-cards-stats";
import { useUserData } from '../../stores/user.data.store';
import { motion } from "framer-motion";

function AsideNav() {
  const userEmail = useUserData((state) => state.userEmail);

  return (
    <motion.aside className='w-[17%] h-[100vh] flex flex-col py-12 px-8 gap-10 sticky top-0 bg-white rounded-tr-3xl rounded-br-3xl'
    initial={{ opacity: 0, y: 10 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 0.5, ease: "easeInOut" }}>
        <article className='flex items-center gap-2'>
            <img className='w-[60px]' src={logoSat} alt="Logo del Servicio de Administración Tributaria" />
            <h3 className='font-helvetica-bold text-3xl'>SAT <span className='font-helvetica-bold text-sat text-3xl'>Learn</span></h3>
        </article>

        <article className='flex flex-col gap-7'>
            <h4 className='font-helvetica-bold text-xl text-slate-900'>Navegación</h4>
            <Nav></Nav>
        </article>

        <article className='flex flex-col gap-7'>
            <h4 className='font-helvetica-bold text-xl text-slate-900'>Tus estadísticas</h4>
            <section className='flex justify-between flex-wrap gap-5'>
              {
                objectCardsStats.map(elementCard => {
                  const { icon, title, value, iconColor } = elementCard;
                  return <CardSat key={title} icon={icon} iconColor={iconColor} title={title} value={value} />
                })
              }
            </section>
        </article>


    </motion.aside>
  )
}

export default AsideNav