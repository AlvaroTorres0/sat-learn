import React, { useEffect, useState } from 'react'
import { useUserData } from "../../../stores/user.data.store";
import axios from "axios";
import 'ldrs/tailspin'

function CardStat({ icon, iconColor, title }) {
  const userEmail = useUserData((state) => state.userEmail);
  const [count, setCount] = useState(null);
  useEffect(() => {
    if (userEmail) {
      const endPoint = `http://localhost:3000/stats/${title}/${userEmail}`;
  
      const fetchData = async () => {
        try {
          const response = await axios.get(endPoint);

          (response.data > 0) ? setCount(response.data) : setCount(0);
        } catch (error) {
          console.error('Error fetching data:', error);
        }
      };
      fetchData();
    }

  },[userEmail])


  return (
    <div className='flex items-center justify-around w-full rounded-xl bg-accent px-6 py-4'>
        {
          count !== null ? (
            <>
              <div className='w-1/3'>
                <i className={icon} style={{ color: iconColor, fontSize: "23px" }}></i>
              </div>

              {
                count === 0 ? (
                  <span className='font-helvetica-light leading-tight'>Sin {title} 👐</span>
                ) : (
                  <div className='w-2/3 flex flex-col'>
                    <span className='font-helvetica-medium text-base'>{title}</span>
                    <span className='font-helvetica-light text-xl'>{count}</span>
                  </div>
                )
              }
            </>
          ) : (
            <l-tailspin size="30" stroke="2" speed="1" color="#00529E"></l-tailspin>
          )
        }
        
    </div>
  )
}

export default CardStat