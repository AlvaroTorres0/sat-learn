export const objectCardsStats = [
    {
        icon: "ti ti-flag-check",
        iconColor: "#37AD24",
        title: 'Terminados',
        value: 0,
    },
    {
        icon: "ti ti-calendar-due",
        iconColor: "#B446B7",
        title: 'Pendientes',
        value: 0,
    },
    {
        icon: "ti ti-heart",
        iconColor: "#FF0000",
        title: 'Favoritos',
        value: 0,
    },
];