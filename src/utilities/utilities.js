import { supabaseClient } from "../supabase/supabaseClient";
import { format } from "date-fns";
import { es } from 'date-fns/locale';

export const getDataInstructor = async (correoInstructor) => {
    try {
        const { data: USUARIO_DATA, error } = await supabaseClient
        .from('usuarios')
        .select('*')
        .eq('correo', correoInstructor);

        if (error) throw new Error;
        if (USUARIO_DATA) return USUARIO_DATA[0]; 
           
    } catch (error) {
        return null;
    }
}

export const searchDataInstructor = async (correo) => {
    try {
      const dataInstructorSupabase = await getDataInstructor(correo);
      return dataInstructorSupabase;
    } catch (error) {
      return null;
    }
}

export const getInstructorName = (dataInstructor) => {
    const { nombre = '', apepat = '', apemat = '' } = dataInstructor;
    return `${nombre} ${apepat} ${apemat}`;
}


export const destructureDate = (date, includeNameDay) => {
    const dateObject = new Date(date);
    if (includeNameDay) return format(dateObject, "EEEE, d 'de' MMMM 'de' yyyy", { locale: es })
    else return format(dateObject, "d 'de' MMMM 'de' yyyy", { locale: es });
}

export const destructureHours = (date) => {
    const dateObject = new Date(date);
    return format(dateObject, 'HH:mm', { locale: es });;
}