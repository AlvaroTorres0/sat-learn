import { React, useState } from 'react'
import videoLogin from "../assets/video-login.mp4";
import logoSat from "../assets/logo-sat.png";
import { Link } from "react-router-dom";
import { supabaseClient } from "../supabase/supabaseClient";
import { toast, Toaster } from "react-hot-toast";

export default function Login() {
  const [correo, setCorreo] = useState('');

  //* Muestra la notificación de envío de correo e invoca la función para enviar el correo
  async function signIn() {
    toast.promise(
      sendEmailAccess(),
      {
        loading: 'Enviando acceso...',
        success: <b>Acceso enviado a tu correo electrónico!</b>,
        error: <b>Dirección de correo no encontrada</b>,
      }
    );
    
  }

  //* Envía el correo de acceso mediante OTP
  async function sendEmailAccess(){
    try {
      const { data, error } = await supabaseClient.auth.signInWithOtp({
        email: correo,
        options: {
          emailRedirectTo: '/home'
        }
      });
  
      if (error) {
        console.error('Error al iniciar sesión:', error.message);
        throw new Error();
      }
    } catch (error) {
      console.error('Error inesperado:', error);
    }
  }
  


  return (
    <section className='desktop:flex'>
      <section className='hidden desktop:flex flex-col desktop:w-[30vw]'>
        <article className='h-[70%] grid place-items-center p-10 bg-gradient-to-r from-cyan-500 to-blue-500'>
          <h2 className='text-4xl text-slate-100 font-helvetica-light'>La plataforma de cursos que necesitabas</h2>
        </article>

        <article className='h-[30%]'>
          <video className='w-full h-full object-cover' src={videoLogin} muted autoPlay loop></video>
        </article>
      </section>


      <section className='h-screen w-screen grid place-items-center desktop:w-[70vw]'>
        <Toaster position='top-center'></Toaster>
        <div className='h-[80%] w-[80%] p-8 flex flex-col justify-center gap-[5%] tablet:gap-[6%] desktop:w-[50%]'>
          <div className='flex flex-col items-center gap-4'>
            <img src={logoSat} className='w-[100px] tablet:w-[130px]' alt="Logo del Servicio de Administración Tributaria" />
            <span className='text-4xl font-helvetica-bold'>¡Bienvenido!</span>
          </div>

          <div className='flex flex-col gap-2'>
            <span className='text-2xl font-helvetica-light text-center text-primary tablet:font-helvetica-bold'>Inicia sesión en SAT <span className='text-sat'>Learn</span></span>
            <span className='font-helvetica-light text-center'>¿No tienes cuenta? <Link className='font-helvetica-bold' to="/registro">Crea una aquí</Link></span>
          </div>

         <div className='flex flex-col gap-8'>
            <article className='flex flex-col gap-2'>
              <label className='text-primary opacity-70'>Correo institucional</label>
              <input onChange={(e) => setCorreo(e.target.value)} value={correo} type="text" className='border rounded-[15px] py-3 px-5 outline-none focus:border-sat' placeholder='Ejemplo: EITA0123O' required />
            </article>

            <button onClick={() => signIn()} className='bg-sat text-center font-helvetica-bold p-3 text-white rounded-[15px]'>Enviar Acceso</button>
         </div>

          <span className='font-helvetica-light text-center'>¿Olvidaste tu contraseña? <span className='font-helvetica-bold'>da clic aquí</span></span>
          
        </div>
      </section>


    </section>
  )
}
