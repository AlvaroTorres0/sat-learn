export const animations = {
    showFirstFrame: {
      x: "0%",
    },
    dismissFirstFrame: {
      x: "-100%",
      display: "none",
      transition: {
        duration: 0.2,
      },
    },
    hideSecondFrame: {
      x: "100%",
      display: "none",
    },
    showSecondFrame: {
      x: "0%",
      transition: {
        delay: 0.2,
        duration: 0.3,
      },
    },
  };
  