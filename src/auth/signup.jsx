import { React, useState } from 'react'
import { motion } from "framer-motion";
import { animations } from './signup.animations.js';
import toast, { Toaster } from 'react-hot-toast';
import { supabaseClient } from "../supabase/supabaseClient.js";
import axios from "axios";

export function Signup() {
  const [firstFrameStatus, setFirstFrameStatus] = useState(true);
  const [rfc, setRFC] = useState('');
  const [password, setPassword] = useState('');
  const [userInfo, setUserInfo] = useState({});
  const changeFrames = () => {
    setFirstFrameStatus(!firstFrameStatus);
  }

  //* Valida si el usuario existe en la base de datos principal (la simulación del AD del SAT)
  async function getDataUserInPrincipalDb(){
    try {
      if (rfc === '' || password === '') {
        toast.error('Ambos campos son obligatorios', { icon: '❗' });
      }else{
        if (!supabaseClient) {
          toast.error("Ha habido un error, intenta más tarde");
          throw new Error('Supabase client not initialized');
        }
  
        const { data: PRINCIPAL_RESPONSE, error: ERROR_PRINCIPAL_QUERY } = await supabaseClient
          .from('sat_users')
          .select('*')
          .eq('rfccorto', rfc);

          if (ERROR_PRINCIPAL_QUERY) {
            throw new Error(ERROR_PRINCIPAL_QUERY.message);
          }

          if(PRINCIPAL_RESPONSE && PRINCIPAL_RESPONSE.length > 0){
            return PRINCIPAL_RESPONSE[0];
          }else{
            return null;
          }
      }
    } catch (error) {
      toast.error("Error inesperado" + error);
      
    }
  }

  //* Registra al usuario en la base de datos
  async function registerNewUser(){
    if (rfc === '' || password === '') {
      toast.error('Ambos campos son obligatorios', { icon: '❗' });
    } else {
      if (!supabaseClient) {
        toast.error("Ha habido un error, intenta más tarde");
        throw new Error('Supabase client not initialized');
      }

      const userData = await getDataUserInPrincipalDb();
      if (userData !== null && userData !== undefined) {
        const isUserRegistered = await validateIfUserIsRegistered(userData.correo);
        setUserInfo(userData);

        //* Valida si el usuario ya se encuentra registrado en la base de datos de la aplicación, si no, envía el correo de verificación
        if (isUserRegistered.exists === false) {
          changeFrames();
          toast.promise(
            sendVerificationEmail(userData.correo, password),
            {
              loading: 'Enviando correo de verificación...',
              success: <b>Correo enviado!</b>,
              error: <b>Ooop, algo ha salido mal</b>,
            }
          );
          await insertInDb(userData);
        } else{
          toast.error('Este RFC ya se encuentra registrado');
        }

      } else {
        toast.error('RFC no encontrado');
        setRFC('');
        setPassword('');
      }
    }
  }

  async function validateIfUserIsRegistered(email){
    try {
      const response = await axios.post('http://localhost:3000/signup/validation/userRegistered', {
        email: email
      });
      return response.data;

    } catch(err){
      
    }
  }

  async function sendVerificationEmail(email, password){
    try {
      if (!supabaseClient) {
        toast.error("No es posible enviar correos de verificación en este momento");
        throw err;
      }
  
      const { data, error } = await supabaseClient.auth.signUp({
        email: email,
        password: password,
      })
  
    } catch (err) {
      toast.error("Error al enviar el correo de verificación, intenta más tarde");
      throw err;
    }
  }

  async function insertInDb(userData){
    try {
      const response = await axios.post('http://localhost:3000/signup/register', {
        userData: userData,
      });
  
    } catch (err) {
      throw err;
    }
  }


  return (
    <article className='flex w-screen h-screen'>
      <Toaster position='top-center'/>
      <motion.section animate={firstFrameStatus ? animations.showFirstFrame : animations.dismissFirstFrame} variants={animations} className='w-[40%] h-full flex flex-col justify-center gap-8 p-10'>
        <div>
            <h2 className='text-[38px] font-helvetica-bold'>Crea tu cuenta en SAT <span className='text-sat'>Learn</span></h2>
            <h3 className='text-xl font-helvetica-light'>Cientos de cursos te esperan</h3>
        </div>

        <form className='flex flex-col gap-6'>
            <div className='flex flex-col gap-2'>
              <label className='text-primary text-lg font-helvetica-medium opacity-70'>Ingresa tu RFC corto</label>
              <input onChange={(e) => setRFC(e.target.value)} value={rfc} type="text" className='border rounded-[15px] py-3 px-5 outline-none focus:border-sat' placeholder='Ejemplo: EITA0123O' required />
            </div>
            <div className='flex flex-col gap-2'>
              <label className='text-primary text-lg font-helvetica-medium opacity-70'>Digita una contraseña</label>
              <input onChange={(e) => setPassword(e.target.value)} value={password} type="password" className='border rounded-[15px] py-3 px-5 outline-none focus:border-sat' placeholder='********' required />
            </div>
        </form>
        <motion.button onClick={() =>registerNewUser()} whileTap={{ scale: 0.9 }} className='rounded-xl bg-sat text-white font-helvetica-bold py-3'>Continuar</motion.button>
      </motion.section>
      

      <motion.section animate={firstFrameStatus ? animations.hideSecondFrame : animations.showSecondFrame} variants={animations} className='w-[40%] h-full flex flex-col justify-center gap-8 p-10'>
        <div className='flex flex-col gap-5'>
            <h2 className='text-[38px] font-helvetica-bold leading-none'><span className='text-sat'>¡Hola {userInfo.nombre}!</span> Estás a un solo paso...</h2>
            <h3 className='text-xl font-helvetica-light'>Hemos enviado un correo electrónico de verificación a tu correo institucional: <span className='font-helvetica-bold'>{userInfo.correo}</span></h3>
        </div>
      </motion.section>

      <section className='w-[60%] relative h-full bg-signup bg-cover bg-no-repeat bg-center px-[5%]'>
        <span className='text-5xl font-helvetica-light text-white absolute bottom-[30%]'>Descubre todo lo que tenemos para ti</span>
      </section>
    </article>
  )
}