/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        'helvetica-bold': ['helvetica-bold'],
        'helvetica-light': ['helvetica-light'],
        'helvetica-medium': ['helvetica-medium'],
        'helvetica-extra-light': ['helvetica-extra-light'],
      },
      textColor:{
        'primary': '#121212',
        'sat': '#00529E',
      },
      colors: {
        'principal': "#F5F6FB",
        'sat': '#00529E',
        'sat-hover': "#0053b2",
        'accent': '#F9F9F9',
        'text-accent': '#5D5D5D',
        'text-primary': '#4E4E4E',
      },
      screens: {
        'tablet': '585px',
        'desktop': '1200px',
      },
      backgroundImage: {
        'card-popular': "url('./cards-cursos/logo.jpg')",
        'signup': "url('./assets/signup.jpg')",
      },
    },
  },
  plugins: [],
}